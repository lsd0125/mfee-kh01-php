<?php

$a = 5;
$b = 7;

echo $a > $b;  // false, 輸出到頁面時會變成空字串
echo '<br>';

echo $a < $b;  // true, 輸出到頁面時會變成
echo '<br>';

echo $a > $b ? 'TRUE' : 'FALSE';
echo '<br>';

