<?php
include __DIR__ . '/../parts/connect_db.php';

$stmt = $pdo->query("SELECT * FROM address_book LIMIT 10");

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../fontawsome/css/all.css">
    <title>Document</title>
</head>

<body>
    <div class="continer">
        <div class="row">
            <div class="col">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">姓名</th>
                            <th scope="col">手機</th>
                            <th scope="col">email</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php while ($i=$stmt->fetch()) :  ?>
                            <tr>
                                <td><?= $i['sid'] ?></td>
                                <td><?= $i['name'] ?></td>
                                <td><?= $i['mobile'] ?></td>
                                <td><?= $i['email'] ?></td>
                            </tr>
                        <?php endwhile; ?>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

    <script src="../bootstrap/js/bootstrap.js"></script>
</body>

</html>