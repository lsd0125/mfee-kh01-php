<?php
session_start();

$users = [
    'ming' => [
        'password' => '1234',
        'nickname' => '小明',
    ],
    'shin' => [
        'password' => '5678',
        'nickname' => '小新',
    ],
];

$code = 0;
$msg = '';


// 判斷有沒有輸入帳號
if( isset($_POST['account']) ){
    $code = 100;
    // 帳號當 key 有沒有對應到 array
    if( ! empty($users[$_POST['account']]) ){
        $code = 200;
        // 帳號對應到的 array, 裡面 password 是不是和輸入的 password 相同
        if( $users[$_POST['account']]['password']===$_POST['password'] ){
            $code = 300;
            // 設定 session --------------
            $_SESSION['user'] = [
                'account' => $_POST['account'],
                'nickname' => $users[$_POST['account']]['nickname']
            ];
        } else {
            $msg = '密碼錯誤';
        }
    } else {
        $msg = '帳號錯誤'; 
    }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../fontawsome/css/all.css">
    <title>Document<?= $code ?></title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <?php if($msg): ?>
                    <div class="alert alert-danger" role="alert">
                    <?= $msg ?>
                    </div>
                <?php endif ?>
                <?php if(! empty($_SESSION['user'])): ?>
                    <div><?= $_SESSION['user']['nickname'] ?> 您好
                    <p><a href="logout-user.php">登出</a></p>    
                </div>
                <?php else: ?>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">登入</h5>
                        <form method="post">
                            <div class="mb-3">
                                <label for="account" class="form-label">帳號</label>
                                <input type="text" class="form-control" id="account" name="account">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">密碼</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                            <button type="submit" class="btn btn-primary">登入</button>
                        </form>
                        
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>





    </div>



    
    <script src="../bootstrap/js/bootstrap.js"></script>
</body>
</html>