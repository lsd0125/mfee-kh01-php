<?php

$a = 7;
$b = 8;

printf("%s * %s = %s", $a, $b, $a*$b);
echo '<br>';

$str = sprintf("%s * %s = %s",$b, $a, $a*$b); // 不直接輸出到頁面

echo "$str<br>";

$n = 12;

printf("%X <br>", $n);
printf("%'02X <br>", $n);