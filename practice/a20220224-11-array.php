<pre>
<?php
// 索引式陣列
$ar = array(2, 5, 7, 8);

$ar2 = [1,3,5,7];

$ar[] = 'hi';  // array push

print_r($ar);

var_dump($ar);

// terminal 執行
// C:\xampp\php\php.exe practice\a20220224-11-array.php
?>
</pre>