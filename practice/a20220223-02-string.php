<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
    $str1 = "Hello";
    $str2 = "dfgd
        dfgdf
        dfg
        dfgd";  // 中間可以換行
    $str3 = 'hi
    泥好';

    echo "$str1 World<br>";
    echo '$str1 World<br>';
    echo "{$str1}oo World<br>";
    echo "${str2}oo World<br>";
    echo '$str1oo World<br>';

    /*
        error: 嚴重的錯誤,程式會立即停止
        warning: 警告, 程式不會停止
        notice: 提示, 程式不會停止
    */

    echo ">\"< \n\n<br>";
    echo '>"< \n\n<br>';
    echo ' \' \\ ';

?>
    
    &nbsp;
    &lt;
</body>
</html>