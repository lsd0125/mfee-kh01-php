<?php

if(! empty($_FILES) and ! empty($_FILES['myFile'])){
    if(
        move_uploaded_file(
            $_FILES['myFile']['tmp_name'], 
            __DIR__. '/'. $_FILES['myFile']['name']
            )
    ){
        echo 'ok';
        exit;
    }

}
echo 'fail';

/*
// 選擇一個檔案時
{
  "myFile": {
    "name": "ironman.jpg",
    "type": "image/jpeg",
    "tmp_name": "C:\\xampp\\tmp\\phpE84C.tmp",
    "error": 0,
    "size": 217421
  }
}

//
{
  "myFile": {
    "name": [
      "ironman.jpg",
      "logo.png"
    ],
    "type": [
      "image/jpeg",
      "image/png"
    ],
    "tmp_name": [
      "C:\\xampp\\tmp\\phpB6C1.tmp",
      "C:\\xampp\\tmp\\phpB6C2.tmp"
    ],
    "error": [
      0,
      0
    ],
    "size": [
      217421,
      15699
    ]
  }
}
*/