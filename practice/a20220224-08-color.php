<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        td {
            width: 50px;
            height: 50px;
        }
    </style>
</head>
<body>

<table>
    <?php for($r=0; $r<16; $r++): ?>
    <tr>
        <?php for($g=0; $g<16; $g++): ?>
            <td style="background-color: <?= sprintf('#%X%X0', $r, $g) ?>">&nbsp;</td>
        <?php endfor; ?>
    </tr>
    <?php endfor; ?>
</table>

<?php /*
<table>
    <?php for($r=0; $r<16; $r++): ?>
    <tr>
        <?php for($g=0; $g<16; $g++): ?>
            <td style="background-color: <?= sprintf('#%X%X0', $r, $g) ?>">&nbsp;</td>
        <?php endfor; ?>
    </tr>
    <?php endfor; ?>
</table>
*/ ?>
</body>
</html>