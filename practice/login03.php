<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../fontawsome/css/all.css">
    <title>Document</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">登入</h5>
                        <form method="post">
                            <div class="mb-3">
                                <label for="account" class="form-label">帳號</label>
                                <input type="text" class="form-control" id="account" name="account" 
                                value="<?= empty($_POST['account']) ? '' : htmlentities($_POST['account']) ?>">
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">密碼</label>
                                <input type="password" class="form-control" id="password" name="password"
                                value="<?= empty($_POST['password']) ? '' : htmlentities($_POST['password']) ?>">
                            </div>
                            <button type="submit" class="btn btn-primary">登入</button>
                        </form>
                        
                    </div>
                </div>
            </div>
        </div>





    </div>



    
    <script src="../bootstrap/js/bootstrap.js"></script>
</body>
</html>