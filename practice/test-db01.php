<?php
require __DIR__. '/../parts/connect_db.php';
// require: 找不到檔案時會發出 error
// include: 找不到檔案時會發出 warning

$stmt = $pdo->query("SELECT * FROM address_book LIMIT 5");

// print_r( $stmt->fetch() );
// print_r( $stmt->fetchAll() );
echo json_encode($stmt->fetchAll(), JSON_UNESCAPED_UNICODE);
