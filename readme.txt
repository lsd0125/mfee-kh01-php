
內建的變數

1. $_GET: 處理 query string 參數
2. $_POST: 處理表單的參數
3. $_COOKIE: 讀取 cookie (設定使用 setcookie())
4. $_SESSION: 處理 session 資料 (必須使用 session_start() 啟動)

functions

1. isset(): 判斷有沒有設定
2. empty(): 判斷是不是空的 (包含空陣列, 沒有設定過的變數)
3. intval(): 轉換為整數
4. header(): 設定 "回應" 的檔頭

suechUVkFd4ctcv2pBK2


