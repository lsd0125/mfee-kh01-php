<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="./fontawsome/css/all.css">
    <!--
    <title><?= isset($title) ? $title : '預設的抬頭' ?></title>
    -->
    <title><?= $title ?? '預設的抬頭' ?></title>
</head>

<body>